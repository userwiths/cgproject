﻿using System.Linq;
using System.Windows.Forms;
using CGProject.Models;
using CGProject.Models.StateModels;
using CGProject.Models.Interfaces;
using CGProject.Managers;
using System.Drawing.Imaging;
using System.Collections.Generic;
using CGProject.Models.Interfaces.Summarized;
using CGProject.Models.Abstract;
using CGProject.Models.Attributes.Handlers;

namespace CGProject
{
    public class Manager:IFilter<IGeometry>
    {
        private TreeManager treeManager;
        private GraphicsManager graphicManager;
        private ToolStripStatusLabel statusBar;
        
        public static List<IGeometry> clip = new List<IGeometry>();
        public MouseState state { get; set; }
        public ProjectState projectState { get; set; }
        public string SearchTerm
        {
            get {
                return this.treeManager.SearchTerm;
            }
            set
            {
                this.treeManager.SearchTerm = value;
            }
        }

        public IGeometry this[int id] {
            get { return this.projectState.Get(id); }
        }
        
        public Manager(PictureBox box,TreeView treeView, ToolStripStatusLabel status) {
            projectState = new ProjectState(StatusBar);
            this.graphicManager = new GraphicsManager(box);
            state = new MouseState(this.HandleMouse);

            this.statusBar = status;
            Group baseGroup = new Group() { Name = "Window" };
            this.Add(baseGroup);
            this.treeManager = new TreeManager(treeView, projectState.Groups,this);
        }

        public List<IGeometry> Get() {
            return this.projectState.Get();
        }
        public List<IGeometry> Get(string name)
        {
            return projectState.Get(name);
        }
        public void Add(int parentId, IGeometry component) {
            var parent = this[parentId];
            var group = parent as Group;

            if (group != null) {
                group.Add(component);
            }

            graphicManager.Draw(component);
        }
        public void Add(IGeometry item) {
            this.projectState.Add(item);
            this.Register(null,item,this.projectState.Selected);

            //Single draw for single item.
            this.graphicManager.Draw(item);
        }
        public void Remove() {
            var temp = this.Get();

            if (temp.Where(x => x is Group).Count() > 1) {
                return;
            }
            else {
                temp = temp.Where(x => x is Polygon).ToList();
            }

            this.Deselect();

            this.Transaction();
            foreach (var item in temp)
            {
                this.Remove(item.Id);
            }
            this.Commit();
            this.Draw();
        }
        public void Remove(int id) {
            this.Register(this[id], null, this.projectState.Selected);
            if (projectState.IsSelected(id))
            {
                this.Select(id);
            }
            this.projectState.Remove(id);
            this.Draw();
        }
        public void Update(int id, IGeometry item) {
            this.projectState.Update(id,item);
        }

        public void Disown(int id) {
            var elem = this[id];
            foreach (var item in this.projectState.Groups)
            {
                if (item is Group group)
                {
                    group.Disown(elem);
                }
            }
        }
        public void Disown()
        {
            var elements = this.Get();
            this.Deselect();
            foreach (var elem in elements)
            {
                foreach (var item in this.projectState.Groups)
                {
                    if (item is Group group)
                    {
                        group.Disown(elem);
                    }
                }
            }
        }
        public void Select(int id) {
            this.projectState.Select(id);

            if (projectState.IsSelected(id))
            {
                graphicManager.Mark(this[id]);
            }
            else {
                graphicManager.ReDraw(projectState.Groups, this.Get());
            }
        }
        public void Deselect() {
            this.projectState.Deselect();
        }

        public void Copy() {
            clip = this.Get().Select(x=>x.Clone() as IGeometry).ToList();
        }
        public void Paste() {
            this.Transaction();
            foreach (var item in clip)
            {
                Add(item);
            }
            this.Commit();

            clip = new List<IGeometry>();
        }
        public void Cut() {
            clip = this.Get().ToList();
            this.Disown();
        }

        public void Draw()
        {
            this.graphicManager.ReDraw(projectState.Groups,this.Get());
            if (treeManager != null)
            {
                treeManager.Refresh();
            }
        }
        public void Export(string file, ImageFormat format = null)
        {
            if (format == null)
            {
                format = ImageFormat.Png;
            }
            this.graphicManager.map.Save(file, format);
        }

        private void HandleMouse()
        {
            state.DraggedItem = new List<int>();
            Models.Point pressedAt = state.PressedAt;
            Models.Point releasedAt = state.ReleasedAt;

            if (state.MouseButton == MouseButtons.Right) { return; }

            Base component = null;
            var onSide = this.OnSideOfPolygon(out component, pressedAt);
            
            Models.Point onPoint = null;

            if (this.OnPoint(out onPoint, pressedAt))
            {
                state.DraggedItem = new List<int>(){ onPoint.Id };
                return;
            }
            if (onSide != null)
            {
                state.DraggedItem = new List<int>() { onSide.begin.Id, onSide.end.Id };
                return;
            }
            if (this.PointInsidePolygon(out component, pressedAt))
            {
                if (!this.projectState.IsSelected(component.Id))
                {
                    this.Select(component.Id);
                }
                if (releasedAt.X==pressedAt.X && releasedAt.Y==pressedAt.Y)
                {
                    state.Dragging = false;
                    return;
                }
                if (releasedAt.Location.X != 0 &&
                    releasedAt.Location.Y != 0)
                {
                    state.DraggedItem = this.projectState.Get().Select(x=>x.Id).ToList();
                    return;
                }
            }

            this.Deselect();
            state.Valid = false;
        }
        private void StatusBar(int owner,string message)
        {
            var temp = this.projectState.Get(owner);
            if (statusBar != null)
            {
                this.statusBar.Text = message + temp.Name;
            }
        }
        public ICollection<IGeometry> Filter() {
            List<IGeometry> result = new List<IGeometry>();
            object temp = null;
            if (SearchTerm == "") {
                SearchTerm = null;
                return this.projectState.Groups;
            }
            foreach (Group group in projectState.Groups)
            {
                foreach (var prop in group.GetType().GetProperties())
                {
                    temp = SearchHandler.Handle(prop, group, SearchTerm);
                    if (temp!=null)
                    {
                        if (temp.Equals(true))
                        {
                            result.Add(group);
                        }
                    }
                }
                foreach (var comp in group.Components)
                {
                    foreach (var prop in comp.GetType().GetProperties())
                    {
                        temp = SearchHandler.Handle(prop, comp, SearchTerm);
                        if (temp != null)
                        {
                            if (temp.Equals(true))
                            {
                                result.Add(comp);
                            }
                        }
                    }
                }
            }
            return result;
        }
    }
}
