﻿using CGProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CGProject
{
    public partial class CreateShape : Form
    {
        private System.Drawing.Graphics graphics;
        private List<Models.Point> points=new List<Models.Point>();

        public Polygon polygon {
            get => new Polygon(0,0,points.ToArray());
        }
        public DialogResult result { get; set; }

        public CreateShape()
        {
            InitializeComponent();
        }

        private void CreateShape_MouseClick(object sender, MouseEventArgs e)
        {
            points.Add(new Models.Point(e.X, e.Y));

            if (points.Count == 1)
            {
                graphics.DrawLine(Pens.Black, new PointF(e.X, e.Y), new PointF(e.X, e.Y));
            }
            else {
                graphics.DrawLines(Pens.Black,points.Select(x=>x.Location).ToArray());
            }
            pictureBox2.Invalidate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            result = DialogResult.OK;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            result = DialogResult.Cancel;
            this.Close();
        }

        private void CreateShape_Load(object sender, EventArgs e)
        {
            Bitmap map = new Bitmap(pictureBox2.Width, pictureBox2.Height);
            pictureBox2.Image = map;
            graphics = System.Drawing.Graphics.FromImage(map);

            pictureBox2.MouseClick += CreateShape_MouseClick;
        }
    }
}
