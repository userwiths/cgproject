﻿using CGProject.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CGProject.Models.Definition;

namespace CGProject.Models
{
    public class OvalShape : Base {
        public Point Center { get; set; }
        public override Point[] GetPoints()
        {
            return new Point[] { Center };
        }
        protected override void SetPoints(PointF[] points)
        {
            this.Center = new Point(points[0].X, points[0].Y);
        }
    }

    public class Circle: OvalShape
    {
        public float Radius { get; set; }

        public Circle(Point center,float r) {
            this.Radius = r;
            this.Center = center;
        }
        
        public override void Scale(float proportion)
        {
            Radius *= proportion;
        }
        public override bool Contains(Point point, int diff)
        {
            double dist = Math.Sqrt(
                (point.X-Center.X)* (point.X - Center.X)+
                (point.Y - Center.Y)* (point.Y - Center.Y));
            return dist<=Radius+diff && dist>=Radius-diff;
        }
        public override Point GetCenter()
        {
            return this.Center;
        }
        public override void Draw(System.Drawing.Graphics gr)
        {
            float r = Radius;
            gr.DrawEllipse(new Pen(this.BorderColor), Center.X, Center.Y, Radius, Radius);
            this.Scale(0.99f);
            gr.FillEllipse(new SolidBrush(this.FillColor), Center.X, Center.Y, Radius, Radius);
            Radius = r;
        }
    }

    public class Ellipse: OvalShape
    {
        public float Rotation;

        public float XRadius { get; set; }
        public float YRadius { get; set; }

        public Ellipse(PointF center,float x,float y) {
            this.Center = new Point(center.X,center.Y);

            this.XRadius = x;
            this.YRadius = y;
        }
        public override void Rotate(float angle)
        {
            Rotation = angle;
        }
        public override void Rotate(PointF point, float angle)
        {
            Rotate(angle);
        }
        public override void Scale(float proportion)
        {
            XRadius *= proportion;
            YRadius *= proportion;
        }
        public override bool Contains(Point point, int diff)
        {
            double distance =
                ((point.X - Center.X) * (point.X - Center.X) / (XRadius * XRadius)) +
                ((point.Y - Center.Y) * (point.Y - Center.Y) / (YRadius * YRadius));

            return distance <= 1;
        }
        public override void Draw(System.Drawing.Graphics gr)
        {
            float x = XRadius, y = YRadius;
            var origin = gr.Transform;

            gr.Transform.RotateAt(this.Rotation, Center);

            gr.DrawEllipse(new Pen(this.BorderColor), Center.X, Center.Y, XRadius, YRadius);
            this.Scale(0.99f);

            XRadius = x;
            YRadius = y;

            gr.FillEllipse(new SolidBrush(this.FillColor),new RectangleF(Center.X, Center.Y, XRadius, YRadius));
            gr.Transform.RotateAt(-this.Rotation,Center);
        }
        public override void DrawOn(System.Drawing.Graphics gr) {
            var origin = gr.Transform;
            gr.Transform.Rotate(this.Rotation);

            gr.FillEllipse(new SolidBrush(Color.FromArgb(60,Color.Blue)), Center.X, Center.Y, XRadius, YRadius);

            gr.Transform.Rotate(-this.Rotation);
        }
    }
}
