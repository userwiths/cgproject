﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CGProject.Models.Definition;

namespace CGProject.Models.Attributes
{
    class SearchAttribute : System.Attribute
    {
        public string PropertyName { get; set; }
        public PropertyType PType { get; set; }

        public SearchAttribute(string name, PropertyType type)
        {
            this.PropertyName = name;
            this.PType = type;
        }
    }
}
