﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.Attributes.Handlers
{
    public class EditableHandler
    {
        public static void Handle(PropertyInfo property, object instance, object value)
        {
            object propValue = null;

            try
            {
                propValue = property.GetValue(instance);
            }
            catch (Exception ex)
            {
                return;
            }

            foreach (var attribute in property.GetCustomAttributes(true))
            {
                if (attribute is EditableAttribute editableAttr)
                {
                    //if (editableAttr.PType == Definition.PropertyType.Property)
                    {
                        property.GetSetMethod().Invoke(instance, new object[] { value });
                        //}
                        break;
                    }
                }
            }
        }
    }
}
