﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.Attributes.Handlers
{
    public class PrintableHandler
    {
        public static object Handle(PropertyInfo item,object instance) {
            object result = null;
            object value = item.GetValue(instance);
            object[] attributes = null;

            attributes = item.GetCustomAttributes(true);
            
            foreach (var customAttr in attributes)
            {
                if (customAttr is PrintableAttribute attribute) {
                    if (attribute.PType == Definition.PropertyType.Property)
                    {
                        result =
                            value.GetType().
                            GetProperty(attribute.PropertyName).
                            GetValue(value);
                    }
                    else
                    {
                        result =
                            value.GetType().
                            GetMethod(attribute.PropertyName, new Type[] { }).
                            Invoke(value, null);
                    }
                    break;
                }    
            }
            return result;
        }
        public static object[] Handle(PropertyInfo[] items,object instance) {
            object temp = null;
            List<object> result = new List<object>();
            foreach (var item in items)
            {
                temp = Handle(item, instance);
                if (temp != null) {
                    result.Add(temp);
                }
            }
            return result.ToArray();
        }
    }
}
