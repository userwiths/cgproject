﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.Attributes.Handlers
{
    public class SearchHandler
    {
        public static object Handle(PropertyInfo item,object instance,object searchTerm) {
            object result = null;
            object value = null;
            object[] attributes = null;

            attributes = item.GetCustomAttributes(true);

            try
            {
                value=item.GetValue(instance);
            }
            catch (Exception ex) {
                return false;
            }

            foreach (var customAttr in attributes)
            {
                if (customAttr is SearchAttribute attribute) {
                    if (searchTerm == null) {
                        result = true;
                        break;
                    }
                    else if (attribute.PType == Definition.PropertyType.Property)
                    {
                        result =
                            value.
                            GetType().
                            GetProperty(attribute.PropertyName).
                            GetValue(value).
                            Equals(searchTerm);
                    }
                    else
                    {
                        result =
                            value.
                            GetType().
                            GetMethod(attribute.PropertyName,new Type[] { searchTerm.GetType() }).
                            Invoke(value, new object[] { searchTerm });
                    }
                    break;
                }    
            }
            return result;
        }
    }
}
