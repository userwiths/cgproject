﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CGProject.Models.Definition;

namespace CGProject.Models.Attributes
{
    public class PrintableAttribute:System.Attribute
    {
        public string PropertyName { get; set; }
        public PropertyType PType { get; set; }

        public PrintableAttribute(string name, PropertyType type) {
            this.PropertyName = name;
            this.PType = type;
        }
    }
}
