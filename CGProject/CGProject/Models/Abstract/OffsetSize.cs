﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.Abstract
{
    public abstract class OffsetSize:SinglePoint
    {
        //[Printable("")]
        public Point Size { get; set; }

        public override void Scale(float proportion)
        {
            Size.X *=proportion;
            Size.Y *= proportion;
        }
        public override bool Contains(Point point, int diff)
        {
            float minY = Math.Min(Offset.Y + Size.Y - diff, Offset.Y - diff);
            float maxY = Math.Max(Offset.Y + Size.Y + diff, Offset.Y + diff);
            float minX = Math.Min(Offset.X + Size.X - diff, Offset.X - diff);
            float maxX = Math.Max(Offset.X + Size.X + diff, Offset.X + diff);
            return 
                point.X>minX &&
                point.X<maxX &&
                point.Y>minY &&
                point.Y<maxY;
        }

        public override void DrawOn(System.Drawing.Graphics gr)
        {
            gr.FillRectangle(new SolidBrush(Definition.MarkedColor),
                Offset.X,
                Offset.Y,
                Offset.X+Size.X,
                Offset.Y+Size.Y);
        }
    }
}
