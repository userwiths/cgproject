﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.Abstract
{
    public abstract class SinglePoint:Base
    {
        public Point Offset { get; set; }

        public override Point[] GetPoints()
        {
            return new Point[] { Offset };
        }
        protected override void SetPoints(PointF[] points)
        {
            this.Offset.X = points[0].X;
            this.Offset.Y = points[0].Y;
        }
        public override Point GetCenter()
        {
            return this.Offset;
        }
        public override bool Contains(Point point, int diff)
        {
            return Offset.Contains(point, diff);
        }
    }
}
