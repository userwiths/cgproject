﻿using CGProject.Models.Attributes;
using CGProject.Models.Interfaces;
using CGProject.Models.Interfaces.Summarized;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CGProject.Models.Definition;

namespace CGProject.Models.Abstract
{
    public abstract class Base : IGeometry
    {
        protected string _name;
        public int Id { get => this.GetHashCode(); }

        private Color _borderColor;
        private Color _fillColor;

        [Printable("Name",PropertyType.Property)]
        [Search("Name", PropertyType.Property)]
        [Editable]
        public Color BorderColor {
            get {
                return this._borderColor;
            }
            set {
                this._borderColor = value;
            }
        }

        [Search("Name", PropertyType.Property)]
        [Printable("Name", PropertyType.Property)]
        [Editable]
        public Color FillColor
        {
            get
            {
                return this._fillColor;
            }
            set
            {
                this._fillColor = value;
            }
        }

        [Search("Contains", PropertyType.Method)]
        public string Name
        {
            get {
                return this._name;
            }
            set {
                _name = value;
            }
        }

        public Base()
        {
            Name = "#" + this.Id;
            this.FillColor = Color.White;
            this.BorderColor = Color.Black;
        }

        public virtual Point[] GetPoints()
        {
            var properties = this.GetType().GetProperties();
            var result = new List<Point>();

            var pointarr =
                properties.
                Where(x => x.PropertyType == typeof(Point[])).
                Select(x => x.GetValue(this) as Point[]);

            foreach (Point[] points in pointarr)
            {
                result.AddRange(points);
            }

            return result.ToArray();
        }
        protected virtual void SetPoints(PointF[] points)
        {
            int i = 0;
            foreach (var item in
                this.GetType().
                GetProperties().
                Where(x => x.PropertyType == typeof(Point)).
                ToList())
            {
                item.SetValue(this, new Point(points[i++]));
            }
        }

        public virtual Models.Point GetCenter() {
            var temp = this.GetPoints();
            if (temp.Length <= 1)
            {
                return null;
            }

            Point result = null;
            float a = 0, b = 0;
            foreach (var point in temp)
            {
                a += point.X;
                b += point.Y;
            }
            result = new Point(a / temp.Length, b / temp.Length);
            return result;
        }

        #region MOVE
        public void MoveWith(PointF margin)
        {
            var temp = this.GetPoints();
            foreach (Point point in temp)
            {
                point.X+=margin.X;
                point.Y += margin.Y;
            }
            this.SetPoints(temp.Select(x=>x.AsNative()).ToArray());
        }
        public void MoveWith(float x, float y)
        {
            this.MoveWith(new PointF(x, y));
        }
        public void MoveTo(PointF margin)
        {
            var temp = GetCompact();
            foreach (Point point in temp)
            {
                point.X += margin.X;
                point.Y += margin.Y;
            }
            SetPoints(temp.Select(x=>(PointF)x).ToArray());
        }
        public void MoveTo(float x, float y)
        {
            this.MoveTo(new PointF(x, y));
        }
        #endregion

        public virtual bool Contains(Point point, int diff) {
            var points = this.GetPoints();
            float maxX = points.Select(x => x.X).Max();
            float maxY = points.Select(x => x.Y).Max();
            float minX = points.Select(x => x.X).Min();
            float minY = points.Select(x => x.Y).Min();

            bool betweenX = point.X >= minX-diff && point.X <= maxX+diff;
            bool betweenY = point.Y >= minY-diff && point.Y <= maxY+diff;

            Point temp;
            List<Point> check = new List<Point>();
            double sideA, sideB, sideC;
            double cos = 0;
            double result = 0;

            foreach (var pnt in points)
            {
                temp = GetClosest(pnt, check);

                sideA = point.Distance(pnt);
                sideC = pnt.Distance(temp);
                sideB = point.Distance(temp);

                cos =
                    (sideC * sideC -
                    (sideB * sideB + sideA * sideA)) /
                    (-2 * sideA * sideB);
                result += Math.Acos(cos) * (180.0 / Math.PI);
                check.Add(pnt);
            }
            return result >= 180 && betweenX && betweenY;
        }

        public virtual void Draw(System.Drawing.Graphics gr)
        {
            var originPoints = this.GetPoints().Select(x => x.AsNative()).ToArray();
            if (originPoints.Count() == 0)
            {
                return;
            }

            gr.DrawPolygon(new Pen(BorderColor), this.GetPoints().Select(x=>x.AsNative()).ToArray());

            this.Scale(0.99f);
            gr.FillPolygon(new SolidBrush(this.FillColor), this.GetPoints().Select(point=>new PointF(point.X+0.1f, point.Y + 0.1f)).ToArray());
            this.SetPoints(originPoints);
        }
        public virtual void Scale(float proportion)
        {
            var offset = this.GetOffset();
            var currentPoints = this.GetPoints().Select(x=>x.AsNative()).ToArray();
            var matrix = new Matrix();

            matrix.Scale(proportion, proportion);
            matrix.TransformPoints(currentPoints);

            this.SetPoints(currentPoints);
            this.SetPoints(this.GetCompact().Select(x => new PointF(x.X + offset.X, x.Y + offset.Y)).ToArray());
        }
        public virtual void Rotate(float angle)
        {
            var origin = this.GetCenter();
            this.Rotate(origin.AsNative(),angle);
        }
        public virtual void Rotate(PointF point,float angle)
        {
            var currentPoints = this.GetPoints().Select(x=>(PointF)x).ToArray();
            Matrix matrix = new Matrix();
            matrix.RotateAt(angle,point);
            matrix.TransformPoints(currentPoints);

            this.SetPoints(currentPoints);
        }
        
        protected virtual Point GetClosest(Point src, List<Point> check)
        {
            var temp = this.GetPoints().Where(x => !check.Contains(x)).OrderBy(x => x.Distance(src)).ToList();
            var result = temp.FirstOrDefault();
            return result;
        }
        protected List<Point> GetCompact()
        {
            float minX = GetPoints().Select(x => x.X).Min();
            float minY = GetPoints().Select(x => x.Y).Min();

            return GetPoints().Select(x => new Point(x.X - minX, x.Y - minY)).ToList();
        }
        protected Point GetOffset()
        {
            float minX = GetPoints().Select(x => x.X).Min();
            float minY = GetPoints().Select(x => x.Y).Min();

            return new Point(minX,minY);
        }

        public virtual bool OnBorder(Point point) {
            if (this.GetPoints().Length > 0)
            {
                return this.GetPoints().Contains(point);
            }
            return true;
        }
        public virtual void DrawOn(System.Drawing.Graphics gr)
        {
            gr.FillPolygon(
                new SolidBrush(Definition.MarkedColor),
                this.
                GetPoints().
                Select(x=>x.AsNative()).
                ToArray());
        }

        public virtual object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
