﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models
{
    public static class Definition
    {
        public static Color MarkedColor = Color.FromArgb(60, Color.Blue);
        public static Color GetColor(this Color instance,string name)
        {
            return Color.FromName(name);
        }

        public enum PropertyType { Property, Method };
        public enum SHAPE { CIRCLE, ELLIPSE, CURVE,POLYGON,SINGULARITY,NONE };
        public delegate void VoidDelegate();
    }
}
