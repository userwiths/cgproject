﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGProject.Models.Interfaces.Summarized;

namespace CGProject.Models.StateModels
{
    class GroupModel
    {
        public List<GroupModel> Groups { get; set; }
        public List<Polygon> Polygons { get; set; }
        public List<OvalShape> Ovals { get; set; }
        public string Name { get; set; }

        public GroupModel() {
            this.Groups = new List<GroupModel>();
            this.Polygons = new List<Polygon>();
            this.Ovals = new List<OvalShape>();
        }
        public GroupModel(Group group) {
            this.Name = group.Name;
            //Using Components in order to retain the child parent order.
            this.Polygons = 
                group.Components.
                Where(x=>x is Polygon).
                Select(x=>x as Polygon).
                ToList();
            this.Ovals= 
                group.Components.
                Where(x=>x is OvalShape).
                Select(x => x as OvalShape).
                ToList();
            this.Groups =
                group.Components.
                Where(x=>x is Group).
                Select(x => new GroupModel(x as Group)).
                ToList();
        }

        public Group Load() {
            Group result = new Group();
            result.Name = Name;

            foreach (var item in Polygons)
            {
                result.Components.Add(item);
            }
            foreach (var item in Ovals)
            {
                result.Components.Add(item);
            }
            foreach (var group in Groups)
            {
                result.Components.Add(group.Load());
            }

            return result;
        }
    }
}
