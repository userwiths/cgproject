﻿using CGProject.Models.Interfaces;
using CGProject.Models.Interfaces.Summarized;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.StateModels
{
    public class ProjectState
    {
        public delegate void MessagePrintDelegate(int owner,string message);
        public event MessagePrintDelegate MessagePrint;

        private Group _selectedGroup;

        public List<int> Selected;
        public ICollection<IGeometry> Groups;

        public string SaveFile { get; set; }
        public Color BorderColor { get; set; }
        public Color FillColor { get; set; }
        
        public ProjectState(MessagePrintDelegate deleg=null) {
            Selected = new List<int>();
            Groups = new List<IGeometry>();

            SaveFile = "";
            this.BorderColor = Color.Black;
            this.FillColor = Color.White;

            this.MessagePrint = deleg;
        }

        public List<IGeometry> Get() {
            return this.Selected.Select(x=>Get(x)).ToList();
        }
        public IGeometry Get(int id)
        {
            IGeometry result = null;
            foreach (var group in Groups)
            {
                if (group is Group grItem)
                {
                    result = grItem[id];
                }
                if (result != null) {
                    break;
                }
            }
            return result;
        }
        public List<IGeometry> Get(string name) {
            List<IGeometry> result = new List<IGeometry>();
            foreach (var group in Groups)
            {
                if (group.Name.Contains(name))
                {
                    result.Add(group);
                }
                if (group is Group grItem)
                {
                    foreach (var element in grItem.GetAllGroups())
                    {
                        if (element.Name.Contains(name))
                        {
                            result.Add(element);
                        }
                    }
                    foreach (var element in grItem.GetAllPolygons())
                    {
                        if (element.Name.Contains(name))
                        {
                            result.Add(element);
                        }
                    }
                }
            }
            return result;
        }

        public void Add(IGeometry item) {
            if (item is Group)
            {
                bool inferior = (item is Group) && _selectedGroup != null;
                if (Groups.Count == 0)
                {
                    _selectedGroup = item as Group;
                }

                if (inferior)
                {
                    _selectedGroup.Add(item);
                }
                else
                {
                    this.Groups.Add(item as Group);
                }
            }
            else
            {
                if (this.GetSelectedGroups().Count == 0)
                {
                    _selectedGroup.Add(item);
                }
                else
                {
                    foreach (var group in this.GetSelectedGroups())
                    {
                        group.Add(item);
                    }
                }
            }

            this.MessagePrint?.Invoke(item.Id, "added: ");
        }
        public void Update(int id, IGeometry item) {
            IGeometry result =null;
            foreach (var group in Groups)
            {
                if (group.Id == id)
                {
                    result=group;
                }
                if (group is Group grItem)
                {
                    foreach (var comp in grItem.Components)
                    {
                        if (comp.Id == id)
                        {
                            result = comp;
                        }
                    }
                }
            }
            result = item;
        }
        public void Remove(int id) {
            bool isGroup = this.Groups.Select(x => x.Id).Contains(id);
            if (isGroup)
            {
                Groups=Groups.Where(x=>x.Id!=id).ToList();
            }
            this.MessagePrint(id, "deleted: ");
            foreach (var group in Groups)
            {
                if (group is Group grItem)
                {
                    grItem.Remove(id);
                }
            }
        }
        public void Remove() {
            foreach (var selection in this.Selected)
            {
                this.Remove(selection);
            }
        }

        public void Select(int id) {
            if (!this.Exists(id)) {
                return;
            }
            if (this.Selected.Contains(id)) {
                MessagePrint?.Invoke(id, "Deselected: ");
                Selected.Remove(id);
                return;
            }

            var item = this.Get(id);
            if (item is Group) {
                if (this._selectedGroup == null) {
                    _selectedGroup = item as Group;
                }
                if (this._selectedGroup.Id != id) {
                    Selected.Remove(_selectedGroup.Id);
                    _selectedGroup=item as Group;
                }
            }

            this.Selected.Add(id);
            MessagePrint?.Invoke(id, "selected: ");
        }
        public void Deselect() {
            Selected = new List<int>();
        }
        public bool IsSelected(int id)
        {
            var temp = this.Get();

            foreach (var group in temp)
            {
                if (group.Id == id)
                {
                    return true;
                }
                else if (group is Group)
                {
                    if ((group as Group)[id] != null)
                    {
                        return true;
                    }
                }    
            }
            return this.Selected.Contains(id);
        }

        private List<Group> GetSelectedGroups() {
            return Get().Where(x => x is Group).Select(x=>x as Group).ToList();
        }
        private List<Polygon> GetSelectedPolygons()
        {
            return Get().Where(x => x is Polygon).Select(x => x as Polygon).ToList();
        }
        private bool Exists(int id)
        {
            foreach (var group in Groups)
            {
                if (group is Group grItem)
                {
                    if (grItem[id] != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
