﻿using CGProject.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CGProject.Models.StateModels
{
    public class MouseState
    {
        public delegate void HandleMouseDelegate();
        private event HandleMouseDelegate HandleMouse;

        private bool _valid;

        public bool Valid
        {
            get
            {
                return this._valid;
            }
            set
            {
                this._valid = value;
                if (this._valid)
                {
                    HandleMouse?.Invoke();//Task.Run(new Action(HandleMouse));
                }
            }
        }
        public bool Dragging { get; set; }
        public ICollection<int> DraggedItem { get; set; }
        public MouseButtons MouseButton { get; set; }
        public PointF PrevPosition { get; set; }
        public PointF Position { get; set; }
        public PointF PressedAt { get; set; }
        public PointF ReleasedAt { get; set; }

        public MouseState(HandleMouseDelegate deleg)
        {
            PressedAt = new PointF(0, 0);
            ReleasedAt = new PointF(0, 0);
            MouseButton = MouseButtons.Left;

            PrevPosition = PointF.Empty;
            Position = PointF.Empty;

            DraggedItem = new List<int>();

            Valid = false;
            Dragging = false;
            this.HandleMouse = deleg;
        }

        public override bool Equals(object obj)
        {
            if (obj is MouseState) {
                return (obj as MouseState).PressedAt.Equals(this.PressedAt) && 
                    (obj as MouseState).ReleasedAt.Equals(this.ReleasedAt);
            }
            return base.Equals(obj);
        }
    }
}
