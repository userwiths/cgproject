﻿using CGProject.Models.Interfaces;
using Graphics.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.Models.StateModels
{
    public class ItemChange<T>:IChangeable<T>
    {
        public T Old { get; set; }
        public T New { get; set; }

        public int Changes()
        {
            return 1;
        }

        public object GetArguments()
        {
            return null;
        }

        public Tuple<T, T> GetChange()
        {
            return new Tuple<T, T>(New,Old);
        }

        public bool HasChange()
        {
            if (Old == null) {
                return New != null;
            }
            bool result=!Old.Equals(New);
            Old = New;
            return result;
        }
    }
    public class ItemChange<T,V> : IChangeable<T,V>
    {
        public T Old { get; set; }
        public T New { get; set; }
        public V Arguments { get; set; }

        public int Changes()
        {
            return 1;
        }

        public V GetArguments()
        {
            return Arguments;
        }

        public Tuple<T, T> GetChange()
        {
            return new Tuple<T, T>(New, Old);
        }

        public bool HasChange()
        {
            if (Old == null) {
                return New != null;
            }
            bool result = !Old.Equals(New);
            Old = New;
            return result;
        }
    }
    public class MultiItemChange<T> : IChangeable<T>
    {
        private int _cursor=0;

        public List<T> OldItems { get; set; }
        public List<T> NewItems { get; set; }

        public MultiItemChange() {
            OldItems = new List<T>();
            NewItems = new List<T>();
        }

        public int Changes()
        {
            return Math.Max(OldItems.Count, NewItems.Count) - _cursor;
        }

        public object GetArguments()
        {
            return null ;
        }

        public Tuple<T, T> GetChange()
        {
            if (this.HasChange()) {
                _cursor += 1;
                return new Tuple<T, T>(NewItems[_cursor],OldItems[_cursor]);
            }
            return null;
        }

        public bool HasChange()
        {
            return _cursor < Math.Max(OldItems.Count, NewItems.Count);
        }
    }
    public class MultiItemChange<T,V> : IChangeable<T,V>
    {
        private int _cursor = 0;

        public List<T> OldItems { get; set; }
        public List<T> NewItems { get; set; }
        public V Arguments { get; set; }

        public MultiItemChange() {
            OldItems = new List<T>();
            NewItems = new List<T>();
        }

        public int Changes()
        {
            return Math.Max(OldItems.Count,NewItems.Count) - _cursor;
        }

        public V GetArguments()
        {
            return Arguments;
        }

        public Tuple<T, T> GetChange()
        {
            Tuple<T, T> result = null;
            if (this.HasChange())
            {
                result= new Tuple<T, T>(OldItems[_cursor], NewItems[_cursor]);
                _cursor++;
            }
            return result;
        }

        public bool HasChange()
        {
            return _cursor < Math.Max(OldItems.Count,NewItems.Count);
        }
    }
    
    public class ActionManager<T>
    {
        private IList<T> container;

        private bool _register = true;
        private int _current = 0;
        private MultiItemChange<T> multiItem = new MultiItemChange<T>();
        private bool AutoCommit = true;
        private List<IChangeable<T>> changes =
            new List<IChangeable<T>>();

        public ActionManager(IList<T> collection)
        {
            container = collection;
        }

        public void Register(T old, T nwItem)
        {
            if (!_register) { return; }
            if (!AutoCommit)
            {
                multiItem.OldItems.Add(old);
                multiItem.NewItems.Add(nwItem);
                return;
            }

            var register = new ItemChange<T>() { Old = old, New = nwItem };
            if (_current > 0)
            {
                changes.RemoveRange(0, _current);
                _current = 0;
            }
            changes.Insert(0, register);
        }
        public void Transaction()
        {
            AutoCommit = false;
        }
        public void Commit()
        {
            changes.Add(multiItem);
            AutoCommit = true;
        }
        public void Undo()
        {
            //Set initial values
            var state = changes[_current];
            var items = state.GetChange();

            _current++;

            if (_current > changes.Count)
            {
                _current--;
                return;
            }

            _register = false;
            if (items.Item1 == null && items.Item2 != null)
            {
                container.Add(items.Item2);
            }
            else if (items.Item1 != null && items.Item2 == null)
            {
                container.Remove(items.Item1);
            }
            else if (items.Item1 != null && items.Item2 != null)
            {
                container.Insert(container.IndexOf(items.Item1), items.Item2);
                container.Remove(items.Item1);
            }
            _register = true;
        }
        public void Redo()
        {
            IChangeable<T> state;

            _current--;
            if (_current < 0)
            {
                _current = 0;
                return;
            }

            state = changes[_current];
            var items = state.GetChange();

            _register = false;
            if (items.Item1 == null && items.Item2 != null)
            {
                container.Remove(items.Item2);
            }
            else if (items.Item1 != null && items.Item2 == null)
            {
                container.Add(items.Item1);
            }
            else if (items.Item1 != null && items.Item2 != null)
            {
                container.Insert(container.IndexOf(items.Item2), items.Item1);
                container.Remove(items.Item2);
            }
            _register = true;
        }
    }
}
