﻿using CGProject.Models.Abstract;
using CGProject.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CGProject.Models.Definition;

namespace CGProject.Models
{
    public class Text : OffsetSize
    {
        [Printable("ToString", PropertyType.Method)]
        [Search("Contains",PropertyType.Method)]
        [Editable]
        public string Message { get; set; }

        [Printable("Name", PropertyType.Property)]
        [Search("Name", PropertyType.Property)]
        public Font Font { get; set; }

        public Text(string msg)
        {
            Message = msg;

            this.Offset = new Models.Point(150, 150);
            this.Size = new Point(50,50);
            this.Font = SystemFonts.DefaultFont;
        }

        public override void Draw(System.Drawing.Graphics gr)
        {
            gr.DrawString(this.Message, this.Font, new SolidBrush(this.BorderColor), this.GetArea());
            
        }

        private RectangleF GetArea() {
            return new RectangleF(Offset.X, Offset.Y, Size.X, Size.Y);
        }
    }
}
