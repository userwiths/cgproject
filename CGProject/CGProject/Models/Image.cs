﻿using CGProject.Models.Abstract;
using CGProject.Models.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CGProject.Models.Definition;

namespace CGProject.Models
{
    public class Image : OffsetSize
    {
        private string _imagePath;

        [Printable("ToString", PropertyType.Method)]
        [Search("Contains",PropertyType.Method)]
        public string ImagePath {
            get { return _imagePath; }
            set
            {
                _imagePath = value;
                if (File.Exists(_imagePath))
                {
                    ImageItem = System.Drawing.Image.FromFile(_imagePath);
                }
            }
        }
        public System.Drawing.Image ImageItem { get; set; }

        public Image(string path)
        {
            ImagePath = path;
        }

        public override void Draw(System.Drawing.Graphics gr)
        {
            gr.DrawImage(this.ImageItem, Offset.X, Offset.Y, Size.X, Size.Y);//base.Draw(gr);
        }
    }
}
