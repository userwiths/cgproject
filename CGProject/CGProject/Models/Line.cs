﻿using CGProject.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models
{
    public class Line : Base
    {
        private double A { get; set; }
        private double B { get; set; }

        private bool upright = false;
        private double C;

        public Point begin { get; set; }
        public Point end { get; set; }

        public Line(Point a, Point b)
        {
            begin = a;
            end = b;

            if (a.X == b.X)
            {
                upright = true;
                C = a.X;
            }
            else
            {
                A = (end.Location.Y - begin.Location.Y) / (end.Location.X - begin.Location.X);
                B = A * (-begin.Location.X) + begin.Location.Y;
            }
        }
        public Line(PointF a, PointF b): this(new Point(a), new Point(b))
        {
        }
        public override Point[] GetPoints()
        {
            return new Point[] { begin,end};
        }
        public override bool Contains(Point point,int diff=2)
        {
            if (upright) {
                return point.X >= C - diff && point.X <= C + diff
                    && point.Y >= Math.Min(begin.Y, end.Y) - diff
                    && point.Y <= Math.Max(begin.Y, end.Y) + diff;
            }

            double resultY = A * point.Location.X + B;
            if (resultY<=point.Location.Y+ diff && resultY >= point.Location.Y - diff)
            {
                return true;
            }
            return false;
        }

        protected override void SetPoints(PointF[] points)
        {
            begin.MoveTo(points[0]);
            end.MoveTo(points[1]);
        }
    }
}
