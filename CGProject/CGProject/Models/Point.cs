﻿using CGProject.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CGProject.Models.Definition;

namespace CGProject.Models
{
    public class Point : Base
    {
        private PointF _location;

        public PointF Location {
            get { return this._location; }
            set {
                this._location = value;
            }
        }
        public float X {
            get { return this._location.X; }
            set {
                this._location.X = value;
            }
        }
        public float Y
        {
            get { return this._location.Y; }
            set
            {
                this._location.Y = value;
            }
        }

        public Point() {
            Location = new PointF(0, 0);
        }
        public Point(float x = 0, float y = 0)
        {
            this.Location = new PointF(x, y);
        }
        public Point(PointF point) {
            Location = point;
        }

        public override Point[] GetPoints()
        {
            return new Point[] { this };
        }
        protected override void SetPoints(PointF[] points)
        {
            this.X = points.FirstOrDefault().X;
            this.Y = points.FirstOrDefault().Y;
        }

        public override bool Contains(Point point,int diff=1)
        {
            return point.X>=this.X-diff && point.X <= this.X + diff
                && point.Y >= this.Y - diff && point.Y <= this.Y + diff;
        }

        public System.Drawing.PointF AsNative()
        {
            return this.Location;
        }
        
        public override bool Equals(object obj)
        {
            if (!(obj is Point other))
            {
                return false;
            }
            return this.X == other.X && this.Y==other.Y;
        }
        public double Distance(Point other)
        {
            if (other == null) {
                return 0.0;
            }
            return Math.Sqrt((this.X + other.X) * (this.X + other.X) +
                (this.Y + other.Y) * (this.Y + other.Y));
        }

        public static implicit operator PointF(Point point)
        {
            return new PointF(point.X, point.Y);
        }
        public static implicit operator Point(PointF point)
        {
            return new Point(point.X, point.Y);
        }
        public static Point operator +(Point a, Point b) {
            return new Point(a.X + b.X, a.Y + b.Y);
        }
        public static Point operator -(Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }
        public static Point operator +(Point a, PointF b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }
        public static Point operator -(Point a, PointF b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }

        public override object Clone()
        {
            return new Point(this.Location);
        }
    }
}
