﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.Models.Interfaces
{
    public interface IChangeableValid {
        bool HasChange();
        int Changes();
    }

    public interface IChangeable: IChangeableValid
    {
        Tuple<object,object> GetChange();
        object GetArguments();
    }

    public interface IChangeable<T,V>: IChangeableValid
    {
        Tuple<T, T> GetChange();
        V GetArguments();
    }

    public interface IChangeable<T>: IChangeableValid
    {
        Tuple<T, T> GetChange();
        object GetArguments();
    }
}
