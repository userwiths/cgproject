﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.Interfaces
{
    public interface IMovable
    {
        void MoveTo(PointF point);
        void MoveTo(float x, float y);
        void MoveWith(PointF point);
        void MoveWith(float x, float y);
    }
}
