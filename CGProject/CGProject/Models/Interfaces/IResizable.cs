﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.Interfaces
{
    public interface IResizable
    {
        void Scale(float proportion);
    }
}
