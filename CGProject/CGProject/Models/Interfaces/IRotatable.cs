﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.Interfaces
{
    public interface IRotatable
    {
        Models.Point GetCenter();
        void Rotate(float angle);
        void Rotate(PointF point,float angle);
    }
}