﻿using CGProject.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.Interfaces.Summarized
{
    public interface IRecognizable: IUnique<int>,INamable
    {
    }
}
