﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Models.Interfaces
{
    public interface IDrawable
    {
        void Draw(System.Drawing.Graphics gr);
        void DrawOn(System.Drawing.Graphics gr);
    }
}
