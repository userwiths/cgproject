﻿using CGProject.Models.Abstract;
using CGProject.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CGProject.Models.Definition;

namespace CGProject.Models
{
    public class Polygon : Base
    {
        public Point[] Points { get; set; }

        public Polygon(float x, float y)
        {
            Points = new Point[] {
                new Point(x,y),
                new Point(x+40,y),
                new Point(x,y+30)
            };
        }
        public Polygon(float x, float y, Point[] points)
        {
            Points = points.Select(z => new Point(x + z.X, y + z.Y)).ToArray();
        }
        public Polygon() : this(0, 0)
        {

        }

        public override Point[] GetPoints()
        {
            return this.Points;
        }
        protected override void SetPoints(PointF[] points)
        {
            for (int i = 0; i < this.Points.Length; i++) {
                this.Points[i].Location = points[i];
            }
        }

        public override object Clone()
        {
            return new Polygon() {
                Points =this.Points,
                BorderColor =this.BorderColor,
                FillColor =this.FillColor
            };
        }
    }
}
