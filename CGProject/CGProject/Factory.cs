﻿using CGProject.Models;
using CGProject.Models.Interfaces;
using CGProject.Models.Interfaces.Summarized;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CGProject.Models.Definition;
using Point = CGProject.Models.Point;

namespace CGProject
{
    public static class GroupGenerator
    {
        public static IGeometry Generate(string element, float x, float y)
        {
            switch (element)
            {
                case "triangle":
                    return new Polygon(x,y);
                case "square":
                    return new Polygon(x, y, new Models.Point[]{
                                new Models.Point(0,0),
                                new Models.Point(40,0),
                                new Models.Point(40,40),
                                new Models.Point(0,40)
                            });
                case "rectangler":
                    return new Polygon(x, y, new Point[]{
                                new Point(0,0),
                                new Point(40,0),
                                new Point(40,20),
                                new Point(0,20)
                            });
                case "circle": return new Circle(new Point(x,y),20);
                case "ellipse":
                    return new Ellipse(new PointF(x,y),50,110);
                case "pentacle":
                    return new Polygon(x, y, new Point[]{
                                new Point(30,0),
                                new Point(70,0),
                                new Point(90,30),
                                new Point(0,30),
                                new Point(60,40)
                            });
            }
            return new Polygon(x, y, new Point[]{
                                new Point(0,0),
                                new Point(40,0),
                                new Point(40,20),
                                new Point(0,20)
                            });
        }
    }
}
