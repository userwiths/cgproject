﻿using CGProject.Managers;
using CGProject.Models;
using CGProject.Models.Abstract;
using CGProject.Models.Attributes.Handlers;
using CGProject.Models.Interfaces.Summarized;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static CGProject.Models.Definition;

namespace CGProject
{
    public partial class Form1 : Form
    {
        private List<Manager> Managers;
        private Manager manager;

        public Form1()
        {
            Managers = new List<Manager>();
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {       
            CreateNewTab();
            manager.Draw();
        }

        private void CreateNewTab(string name="") {
            TabPage page = new TabPage();
            PictureBox pictureBox = new PictureBox();
            Manager nwManager = new Manager(pictureBox, treeView1, toolStripStatusLabel1);

            page.Size = tabControl2.Size;
            if (name == "")
            {
                page.Text = (Managers.Count + 1).ToString();
            }
            else
            {
                page.Text = name;
            }
            
            pictureBox.Size = page.Size;

            pictureBox.MouseWheel += Form1_MouseWheel;
            pictureBox.MouseDown += Form1_MouseDown;
            pictureBox.MouseUp+=Form1_MouseUp;
            pictureBox.MouseMove += Form1_MouseMove;

            page.Controls.Add(pictureBox);
            tabControl2.TabPages.Add(page);

            Managers.Add(nwManager);
            manager = nwManager;

            tabControl2.SelectTab(Managers.Count-1);
        }
        private void FillTab()
        {
            var items = manager.Get();
            foreach (var item in items)
            {
                textBox1.Text = item.BorderColor.Name;
                textBox2.Text = item.FillColor.Name;

                numericUpDown1.Value = decimal.Parse(item.GetCenter().X.ToString());
                numericUpDown2.Value = decimal.Parse(item.GetCenter().Y.ToString());
            }
            numericUpDown3.Value = 1;
            numericUpDown4.Value = 0;
        }

        private void Form1_MouseWheel(object sender, MouseEventArgs e)
        {
            var temp = manager.Get();
            if (temp == null) { return; }
            if (e.Button != MouseButtons.None) {
                return;
            }

            foreach (var item in temp)
            {
                item.Scale(1.0f - (e.Delta / 1000.0f));
            }
            manager.Draw();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) {return;}

            manager.state.PressedAt = new PointF(e.X, e.Y);
            manager.state.PrevPosition = manager.state.PressedAt;
            manager.state.Position = manager.state.PressedAt;

            manager.state.Valid = true;
            manager.state.Dragging = true;
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) { return; }
            
            manager.state.ReleasedAt = new PointF(e.X, e.Y);
            manager.state.Dragging = false;
        }

        private void triangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var item = GroupGenerator.Generate("triangle", 100, 100);
            manager.Add(item);
        }

        private void reactangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var item = GroupGenerator.Generate("rectangler", 200, 130);
            manager.Add(item);
        }

        private void fillColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK) {
                manager.projectState.FillColor = colorDialog1.Color;
            }
        }

        private void drawColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                manager.projectState.BorderColor = colorDialog1.Color;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IOManager.Save(this.manager.projectState);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.manager.projectState = IOManager.Load();
            treeView1.Nodes.Clear();
            this.manager.Draw();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int parentId = 0;
            var parent = manager.Get().FirstOrDefault();
            Group temp = new Group();

            if (manager.Get().FirstOrDefault() is Group)
            {
                parentId = parent.Id;
                this.manager.Add(parentId, temp);
            }
            else
            {
                this.manager.Add(temp);
            }

            this.manager.Draw();

            treeView1.SelectedNode = treeView1.Nodes.Find(temp.Name,true).FirstOrDefault();
            
            treeView1.LabelEdit = true;
            treeView1.SelectedNode.BeginEdit();
        }

        private void treeView1_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            
            treeView1.LabelEdit = false;
            treeView1.SelectedNode.EndEdit(false);
            
            bool isId = int.TryParse(e.Node.Name,out int id);
            var element = isId?this.manager[id] :null;
            TreeNode propertyName;

            string idStr = "";
            
            
            if (element == null) {
                propertyName = e.Node.Parent;
                idStr = propertyName.Parent.Name;
                var temp = manager[int.Parse(idStr)];

                if (propertyName == null) {
                    return;
                }

                if (temp.GetType().GetProperty(propertyName.Text) != null)
                {
                    if (propertyName.Text.Contains("Color")) { 
                        EditableHandler.Handle(
                            temp.GetType().
                            GetProperty(propertyName.Text),
                            temp,
                            Color.FromName(e.Label)
                        );
                    }
                    else
                    {
                        EditableHandler.Handle(
                            temp.GetType().
                            GetProperty(propertyName.Text),
                            temp,
                            e.Label
                        );
                    }
                    manager.Draw();
                }
                return;
            }

            element.Name = e.Label;
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (!int.TryParse(e.Node.Name, out int id)) {
                return;
            }
            var select = this.manager[id];
            if ( select != null)
            {
                this.manager.Select(select.Id);
            }
        }

        private void treeView1_DoubleClick(object sender, EventArgs e)
        {
            bool isId=int.TryParse(treeView1.SelectedNode.Name,out int index);
            var item = manager[index];
            if (treeView1.SelectedNode.Name.Contains("Printable") ||
                item !=null
                ) {
                treeView1.LabelEdit = true;
                treeView1.SelectedNode.BeginEdit();
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            manager.projectState.Remove();
            manager.Draw();
        }

        private void pentacleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var item = GroupGenerator.Generate("pentacle", 200, 130);
            manager.Add(item);
            manager.Draw();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode) {
                case Keys.Left:
                    if (e.Control)
                    {
                        manager.Rotate(-5f);
                    }
                    else
                    {
                        manager.MoveWith(-10,0);
                    }
                    break;
                case Keys.Right:
                    if (e.Control)
                    {
                        manager.Rotate(5f);
                    }
                    else
                    {
                        manager.MoveWith(+10, 0);
                    }
                    break;
                case Keys.Up:
                    manager.MoveWith(0, -10);
                    break;
                case Keys.Down:
                    manager.MoveWith(0, 10);
                    break;
                case Keys.Z:
                    if (e.Control)
                    {
                        manager.Undo();
                    }
                    break;
                case Keys.Y:
                    if (e.Control)
                    {
                        manager.Redo();
                    }
                    break;
                case Keys.C:
                    if (e.Control) {
                        manager.Copy();
                    }
                    break;
                case Keys.V:
                    if (e.Control)
                    {
                        manager.Paste();
                        manager.Draw();
                    }
                    break;
                case Keys.S:
                    if (e.Control) {
                        if (e.Shift) {
                            foreach (var man in Managers)
                            {
                                IOManager.Save(man.projectState);
                            }
                            return;
                        }
                        IOManager.Save(manager.projectState);
                    }
                    break;
            }
            manager.Draw();
        }

        private void shapeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateShape createForm = new CreateShape();
            createForm.ShowDialog();
            if (createForm.result == DialogResult.OK) {
                this.manager.Add(createForm.polygon);
                
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            Models.Point point = new Models.Point(e.X, e.Y);
            Base temp = new Polygon();

            if (e.Button != MouseButtons.None)
            {
                manager.state.Position = new PointF(e.X, e.Y);
                return;
            }

            if (manager.OnPoint(point))
            {
                Cursor.Current = Cursors.Cross;
            }
            else if (manager.OnSide(point))
            {
                Cursor.Current = Cursors.HSplit;
            }
            else if (manager.PointInsidePolygon(out temp, point))
            {
                Cursor.Current = Cursors.Hand;
            }
            else
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void addToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Group temp = new Group();
            this.manager.Add(temp);
            this.manager.Draw();
            foreach (TreeNode item in treeView1.Nodes)
            {
                if (item.Text == temp.Id.ToString())
                {
                    treeView1.SelectedNode = item;
                    break;
                }
            }

            treeView1.LabelEdit = true;
            treeView1.SelectedNode.BeginEdit();
        }

        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode.Name != "") {
                manager.Remove(int.Parse(treeView1.SelectedNode.Name));
            }
        }

        private void pNGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dial = new SaveFileDialog();
            if (dial.ShowDialog() == DialogResult.OK) {
                manager.Export(dial.FileName,ImageFormat.Png);
            }
        }

        private void bitMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dial = new SaveFileDialog();
            if (dial.ShowDialog() == DialogResult.OK)
            {
                manager.Export(dial.FileName, ImageFormat.Bmp);
            }
        }

        private void twoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            manager.Remove();
        }

        private void fillColorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if(colorDialog1.ShowDialog() == DialogResult.OK){
                foreach (var item in manager.Get())
                {
                    item.FillColor = colorDialog1.Color;
                }
            }
        }

        private void borderColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                foreach (var item in manager.Get())
                {
                    item.BorderColor = colorDialog1.Color;
                }
            }
        }

        private void oneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var items = manager.Get().ToArray();
            Group temp = new Group(items);
            foreach (var item in items)
            {
                manager.Disown(item.Id);
            }

            manager.Add(temp);
            
            this.manager.Draw();

            treeView1.SelectedNode=treeView1.Nodes.Find(temp.Id.ToString(),true).FirstOrDefault();
            
            treeView1.LabelEdit = true;
            treeView1.SelectedNode.BeginEdit();
            
        }

        private void ellipseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var temp = GroupGenerator.Generate("ellipse", 200, 200);
            manager.Add(temp);
        }

        private void emptyEvent(object sender, EventArgs e)
        {
        }

        private void circleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            manager.Add(GroupGenerator.Generate("circle",450,200));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(colorDialog1.ShowDialog() == DialogResult.OK){
                foreach (var item in manager.Get())
                {
                    item.BorderColor = colorDialog1.Color;
                }
                manager.Draw();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                foreach (var item in manager.Get())
                {
                    item.FillColor = colorDialog1.Color;
                }
                manager.Draw();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Color selected = Color.FromName(textBox1.Text);
            if (selected.IsKnownColor) {
                foreach (var item in manager.Get())
                {
                    item.BorderColor = selected;
                }
                manager.Draw();
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Color selected = Color.FromName(textBox2.Text);
            if (selected.IsKnownColor)
            {
                foreach (var item in manager.Get())
                {
                    item.FillColor = selected;
                }
                manager.Draw();
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            var old = decimal.Parse(((UpDownBase)sender).Text);
            var nw = numericUpDown1.Value;

            if (old > nw) {
                manager.MoveWith(float.Parse((old-nw).ToString()),0);
            }else
            {
                manager.MoveWith(float.Parse((nw - old).ToString()), 0);
            }
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            var old = decimal.Parse(((UpDownBase)sender).Text);
            var nw = numericUpDown2.Value;

            if (old > nw)
            {
                manager.MoveWith(0,float.Parse((old - nw).ToString()));
            }
            else
            {
                manager.MoveWith(0,float.Parse((nw - old).ToString()));
            }
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            manager.Scale(float.Parse(numericUpDown3.Value.ToString()));
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            manager.Rotate(float.Parse(numericUpDown4.Value.ToString()));
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateNewTab();
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            manager = Managers[tabControl2.SelectedIndex];
            manager.Draw();
        }

        private void loadToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dial = new OpenFileDialog();

            if (dial.ShowDialog() == DialogResult.OK) {
                CreateNewTab(dial.SafeFileName);
                manager.projectState=IOManager.LoadJson(dial.FileName);
                manager.Draw();
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int selected=tabControl2.SelectedIndex;

            tabControl2.TabPages.Remove(tabControl2.SelectedTab);

            Managers.Remove(Managers[selected]);

            manager = Managers[selected] ?? Managers.FirstOrDefault();
        }

        private void emptyEvent(object sender, KeyEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var point = manager.state.Position;
            var prev = manager.state.PrevPosition;

            if (point.X==0 && point.Y==0) {
                return;
            }
            if (manager.state.Dragging && 
                !PointF.Equals(manager.state.PressedAt,manager.state.ReleasedAt)
                )
            {
                manager.state.DraggedItem = manager.state.DraggedItem.Distinct().ToList();
                foreach (var item in manager.state.DraggedItem)
                {
                    if (manager[item] is Group group)
                    {
                        foreach (var comp in group.Components)
                        {
                            comp.MoveWith(point.X - prev.X, point.Y - prev.Y);
                        }
                    }
                    else
                    {
                        manager[item].MoveWith(point.X - prev.X, point.Y - prev.Y);
                    }
                }
                manager.Draw();
            }
            manager.state.PrevPosition = point;
        }

        private void disbandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode selected = treeView1.SelectedNode;
            var selectedId = treeView1.SelectedNode.Name;
            Group selectedGroup = manager[int.Parse(selectedId)] as Group;
            Group parentGroup = null;
            if (selected.Parent != null)
            {
                parentGroup = manager[int.Parse(selected.Parent.Name)] as Group;
                foreach (var comp in selectedGroup.Components)
                {
                    parentGroup.Add(comp);
                }
                selectedGroup.Disown();
                parentGroup.Disown(selectedGroup);
                manager.Draw();
            }
        }

        private void treeView1_DragDrop(object sender, DragEventArgs e)
        {
            //Where the item is dropped.
            System.Drawing.Point targetPoint = treeView1.PointToClient(new System.Drawing.Point(e.X, e.Y));

            TreeNode targetNode = treeView1.GetNodeAt(targetPoint);
            TreeNode draggedNode = (TreeNode)e.Data.GetData(typeof(TreeNode));

            int draggedId = int.Parse(draggedNode.Name);
            int droppedId = int.Parse(targetNode.Name);

            var dragged = manager[draggedId];
            Group dropped = manager[droppedId] as Group;
            if (
                !draggedNode.Equals(targetNode) && 
                targetNode != null &&
                dropped!=null
                )
            {
                draggedNode.Remove();
                manager.Disown(draggedId);
                dropped.Add(dragged);

                manager.Draw();
            }
        }

        private void treeView1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void treeView1_ItemDrag(object sender, ItemDragEventArgs e)
        {
            DoDragDrop(e.Item, DragDropEffects.Move);
        }

        private void TreeSearch_TextChanged(object sender, EventArgs e)
        {
            if (TreeSearch.Text!="") {
                manager.SearchTerm = TreeSearch.Text;
            }
        }

        private void textToolStripMenuItem_Click(object sender, EventArgs e)
        {
            manager.Add(new Text("Hello there general Kenoby"));
        }

    }
}
