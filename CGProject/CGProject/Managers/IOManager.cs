﻿using System.Windows.Forms;

using System.IO;

using Newtonsoft.Json;
using CGProject.Models.StateModels;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Linq;
using CGProject.Models.Interfaces;
using CGProject.Models.Interfaces.Summarized;

namespace CGProject.Managers
{
    public static class IOManager {
        private static JsonSerializer serializer = new JsonSerializer();
        
        public static void Save(ProjectState state) {
            if (state.SaveFile != "") {
                SaveJson(state, state.SaveFile);
                return;
            }

            SaveFileDialog file=new SaveFileDialog();
            file.Filter = "Json File (*.json)|*.json";

            if (file.ShowDialog() == DialogResult.OK)
            {
                state.SaveFile = file.FileName;

                SaveJson(state, file.FileName);
            }
            else {
                state.SaveFile = "temp.json";

                SaveJson(state,"temp.json");
            }
        }
        public static ProjectState Load()
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Json File (*.json)|*.json";

            if (file.ShowDialog() == DialogResult.OK)
            {
                return LoadJson(file.FileName);
            }
            else
            {
                return LoadJson("temp.json");
            }
        }

        public static void SaveJson(ProjectState state,string path = "temp.json") {
            List<GroupModel> result = new List<GroupModel>();
            foreach (var group in state.Groups)
            {
                if (group is Group grItem)
                {
                    result.Add(new GroupModel(grItem));
                }
            }

            StreamWriter writer = new StreamWriter(path);

            serializer.Serialize(writer, result);

            writer.Flush();
            writer.Close();
        }
        public static ProjectState LoadJson(string path="temp.json")
        {
            List<GroupModel> loadResult = new List<GroupModel>();
            ProjectState result = new ProjectState();

            StreamReader streamReader = new StreamReader(path);
            JsonReader reader = new JsonTextReader(new StringReader(streamReader.ReadToEnd()));

            loadResult = serializer.Deserialize<List<GroupModel>>(reader);

            streamReader.Close();
            reader.Close();

            result.Groups = loadResult.Select(x => x.Load() as IGeometry).ToList();

            return result;
        }
    }
}
