﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CGProject.Models;
using CGProject.Models.Abstract;
using CGProject.Models.Attributes;
using CGProject.Models.Attributes.Handlers;
using CGProject.Models.Interfaces;
using CGProject.Models.Interfaces.Summarized;

namespace CGProject
{
    public interface ITreeManager<T> where T:class
    {
        void Refresh();
        void FillTree();
        void AddNodes();
        void RemoveNodes();
        TreeNode ToTreeNode(T item);
    }

    public class TreeManager:ITreeManager<IGeometry> {
        private TreeView _view;
        private string _searchTerm;
        private IFilter<IGeometry> _filter;

        public string SearchTerm {
            get {
                return this._searchTerm;
            }
            set {
                this._searchTerm = value;
                if (_searchTerm == "")
                {
                    _searchTerm = null;
                }
                if (_filter != null)
                {
                    Items = _filter.Filter();
                }
            }
        }
        public ICollection<IGeometry> Items { get; set; }

        public TreeManager(TreeView view) {
            this.SearchTerm = "";
            this._view = view;
            Items = new List<IGeometry>();
        }
        public TreeManager(TreeView view,ICollection<IGeometry> items)
        {
            this.SearchTerm = "";
            this._view = view;
            Items = items;
        }
        public TreeManager(TreeView view, IFilter<IGeometry> filter) : this(view) {
            _filter = filter;
        }
        public TreeManager(TreeView view, ICollection<IGeometry> items, IFilter<IGeometry> filter) : this(view,items)
        {
            _filter = filter;
        }

        public void Refresh() {
            RemoveNodes();
            AddNodes();
        }
        public void FillTree() {
            this.FillTree(this.Items.ToArray());
        }
        public void FillTree(IGeometry[] items)
        {
            _view.BeginUpdate();

            _view.Nodes.Clear();
            foreach (var item in items)
            {
                _view.Nodes.Add(ToTreeNode(item));
            }

            _view.EndUpdate();
        }
        public void AddNodes() {
            var groups = this.Items;
            _view.BeginUpdate();
            foreach (var group in groups)
            {
                if (group is Group grItem)
                {
                    this.PlaceGroupNode(null, grItem);
                }
            }
            _view.EndUpdate();
        }
        public void RemoveNodes()
        {
            var groups = this.Items;
            List<string> componentIds = new List<string>();
            List<string> treeIds = new List<string>();

            TreeNode parent = null;

            foreach (var group in groups)
            {
                if (group is Group grItem)
                {
                    componentIds.AddRange(grItem.GetAll().Select(x => x.ToString()).ToList());
                }
            }

            foreach (TreeNode node in _view.Nodes)
            {
                treeIds.AddRange(CollectNames(node));
            }

            foreach (var item in componentIds)
            {
                if (treeIds.Contains(item))
                {
                    treeIds.Remove(item);
                }
            }

            _view.BeginUpdate();
            foreach (var id in treeIds)
            {
                parent = _view.Nodes.Find(id, true).FirstOrDefault();
                if (parent == null ||
                    parent.Name.Contains("Printable")
                    //Color.FromName(parent.Text).IsKnownColor
                    )
                {
                    continue;
                }
                parent.Remove();
            }
            _view.EndUpdate();
        }
        public TreeNode ToTreeNode(Group group) {
            return new TreeNode(
                    group.Name,
                    group.
                    Components.
                    Select(x => ToTreeNode(x)).
                    ToArray())
            {
                Name = group.Id.ToString()
            };
        }
        public TreeNode ToTreeNode(IGeometry obj)
        {
            TreeNode node = new TreeNode();
            
            if (obj is Group group)
            {
                ToTreeNode(group);
            }
            else if (obj is Base geometry)
            {
                node = new TreeNode(
                    geometry.Name,
                    GeometryTreeNode(geometry)
                    )
                {
                    Name = geometry.Id.ToString()
                };
            }
            return node;
        }

        private TreeNode[] GeometryTreeNode(Base item)
        {
            object value = null;
            string result = "";
            TreeNode temp = null;
            List<TreeNode> nodes = new List<TreeNode>();
            foreach (var prop in item.GetType().GetProperties())
            {
                result = PrintableHandler.Handle(prop, item)?.ToString();
                if (result != null)
                {
                    temp = new TreeNode();
                    temp.Text = prop.Name;
                    temp.Name += "Printable;";
                    value = prop.GetValue(item);

                    temp.Nodes.Add(new TreeNode(
                        result == "" ? "None" : result
                    ));

                    foreach (TreeNode elem in temp.Nodes)
                    {
                        elem.Name += "Printable";
                    }

                    nodes.Add(temp);
                }
            }
            return nodes.ToArray();
        }
        private void PlaceNode(TreeNode node, int parentId) {
            var parent = _view.Nodes.Find(parentId.ToString(), true).FirstOrDefault();
            if (parent == null) {
                return;
            }

            parent.Nodes.Add(node);
        }
        private void PlaceGroupNode(TreeNode parent,Group group) {
            var nodeSet = _view.Nodes;
            if (parent != null) {
                nodeSet = parent.Nodes;
            }

            if (!nodeSet.ContainsKey(group.Id.ToString())) {
                nodeSet.Add(ToTreeNode(group));
            }
            parent = nodeSet[group.Id.ToString()];
            foreach (var comp in group.Components)
            {
                if (comp is Group)
                {
                    PlaceGroupNode(parent, (comp as Group));
                }
                else {
                    if (!parent.Nodes.ContainsKey(comp.Id.ToString())) {
                        parent.Nodes.Add(ToTreeNode(comp));
                    }
                }
            }
        }
        private List<string> CollectNames(TreeNode node) {
            List<string> result = new List<string>();
            foreach (TreeNode item in node.Nodes)
            {
                result.Add(item.Name);
                if (item.Nodes.Count > 0) {
                    result.AddRange(CollectNames(item));
                }
            }
            return result;
        }
    }
}
