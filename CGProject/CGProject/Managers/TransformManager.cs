﻿using CGProject.Models;
using CGProject.Models.Abstract;
using CGProject.Models.Interfaces;
using CGProject.Models.Interfaces.Summarized;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Managers
{
    public static class TransformManager
    {
        public static void Rotate(this Manager manager,float angle) {
            foreach (var element in manager.GetUnique())
            {
                element.Rotate(angle);
            }
            manager.Draw();
        }

        #region MOVE
        public static void MoveTo(this Manager manager, PointF point)
        {
            foreach (var element in manager.GetUnique())
            {
                element.MoveTo(point);
            }
            manager.Draw();
        }
        public static void MoveTo(this Manager manager, float x,float y)
        {
            foreach (var element in manager.GetUnique())
            {
                element.MoveTo(x,y);
            }
            manager.Draw();
        }
        public static void MoveWith(this Manager manager, PointF point)
        {
            foreach (var element in manager.GetUnique())
            {
                element.MoveWith(point);
            }
            manager.Draw();
        }
        public static void MoveWith(this Manager manager, float x, float y)
        {
            foreach (var element in manager.GetUnique())
            {
                element.MoveWith(x, y);
            }
            manager.Draw();
        }
        #endregion

        public static void Scale(this Manager manager, float proportion)
        {
            foreach (var element in manager.GetUnique())
            {
                element.Scale(proportion);
            }
            manager.Draw();
        }

        public static List<IGeometry> GetUnique(this Manager manager) {
            List<IGeometry> result = new List<IGeometry>();
            var allItems = manager.Get();
            foreach (var item in allItems)
            {
                if (item == null) {
                    continue;
                }else if (item is Base)
                {
                    result.Add(item);
                }
                else {
                    result.AddRange((item as Group).GetAllPolygons());
                }
            }
            return result.Distinct().ToList();
        }
    }
}
