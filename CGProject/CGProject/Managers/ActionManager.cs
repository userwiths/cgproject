﻿using CGProject.Models.Interfaces;
using CGProject.Models.StateModels;
using CGProject.Models.Interfaces.Summarized;
using Graphics.Models.Interfaces;
using Graphics.Models.StateModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Managers
{
    public class ItemChange : ItemChange<IGeometry, List<int>>{ }
    public class MultiItemChange : MultiItemChange<IGeometry, List<int>> { }

    public static class ActionManager
    {
        private static bool _register=true;
        private static int _current = 0;
        private static MultiItemChange<IGeometry, List<int>> multiItem = new MultiItemChange<IGeometry, List<int>>();
        private static bool AutoCommit = true;
        private static 
            List<IChangeable<IGeometry,List<int>>> changes=
            new List<IChangeable<IGeometry, List<int>>>();

        public static void Register(this Manager manager, IGeometry old, IGeometry nwItem, List<int> owner)
        {
            if (!_register) { return; }
            if (!AutoCommit) {
                multiItem.OldItems.Add(old);
                multiItem.NewItems.Add(nwItem);
                return;
            }

            var register = new ItemChange() { Arguments = owner, Old = old, New = nwItem};
            if (_current > 0)
            {
                changes.RemoveRange(0, _current);
                _current = 0;
            }
            changes.Insert(0,register);
        }
        public static void Transaction(this Manager manager) {
            AutoCommit = false;
        }
        public static void Commit(this Manager manager) {
            changes.Insert(0,multiItem);
            multiItem = new MultiItemChange<IGeometry, List<int>>();
            AutoCommit = true;
        }
        public static void Undo(this Manager manager) {
            //Set initial values
            var state = changes[_current];
            Tuple<IGeometry, IGeometry> items;

            _current++;

            if (_current > changes.Count) {
                _current--;
                return;
            }

            List<int> tempInt = new List<int>();

            //Set Argumentss of modified objects
            manager.projectState.Selected = state.GetArguments();
            if (manager.projectState.Selected == null) {
                manager.projectState.Selected = tempInt;
            }

            _register = false;
            for(int i=0;i<state.Changes();i++)
            {
                items = state.GetChange();
                if (items.Item1 == null && items.Item2 != null)
                {
                    tempInt = manager.projectState.Selected;

                    manager.Remove(items.Item2.Id);
                }
                else if (items.Item1 != null && items.Item2 == null)
                {
                    tempInt = manager.projectState.Selected;

                    manager.Add(items.Item1);
                }
                else if (items.Item1 != null && items.Item2 != null)
                {
                    tempInt = manager.projectState.Selected;

                    manager.Update(items.Item1.Id, items.Item2);
                }
            }
            _register = true;

            //Set initially selected items;
            manager.projectState.Selected = tempInt;
            manager.Draw();
        }
        public static void Redo(this Manager manager) {
            //Set initial values
            IChangeable<IGeometry, List<int>> state;
            Tuple<IGeometry, IGeometry> items;

            _current--;
            if (_current < 0)
            {
                _current=0;
                return;
            }

            state = changes[_current];
            
            List<int> tempInt = new List<int>();

            //Set Arguments of the modified object.
            manager.projectState.Selected = state.GetArguments();
            if (manager.projectState.Selected == null)
            {
                manager.projectState.Selected = tempInt;
            }

            _register = false;
            for (int i = 0; i < state.Changes(); i++)
            {
                items = state.GetChange();
                if (items.Item1 == null && items.Item2 != null)
                {
                    tempInt = manager.projectState.Selected;

                    manager.Remove(items.Item2.Id);
                }
                else if (items.Item1 != null && items.Item2 == null)
                {
                    tempInt = manager.projectState.Selected;

                    manager.Add(items.Item1);
                }
                else if (items.Item1 != null && items.Item2 != null)
                {
                    tempInt = manager.projectState.Selected;

                    manager.Update(items.Item2.Id, items.Item1);
                }
            }
            _register = true;

            //set initially marked items
            manager.projectState.Selected = tempInt;
            manager.Draw();
        }
    }
}
