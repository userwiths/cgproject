﻿using CGProject.Models.Interfaces.Summarized;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace CGProject.Managers
{
    public class GraphicsManager
    {
        public Bitmap map { get; }
        private PictureBox box;
        private System.Drawing.Graphics graphics;
        
        public GraphicsManager(PictureBox box) {
            this.map = new Bitmap(1000, 650); ;
            this.graphics = System.Drawing.Graphics.FromImage(map);
            this.box = box;
            this.box.Image = this.map;

            this.graphics.CompositingQuality = CompositingQuality.HighQuality;
            this.graphics.SmoothingMode = SmoothingMode.HighQuality;
        }

        public void Draw(IGeometry item)
        {
            item.Draw(this.graphics);
            Refresh();
        }
        public void Mark(IGeometry item) {
            item.DrawOn(graphics);
            Refresh();
        }
        public void Draw(ICollection<IGeometry> item)
        {
            foreach (var elem in item)
            {
                elem.Draw(graphics);
            }
            Refresh();
        }
        public void Mark(ICollection<IGeometry> item)
        {
            foreach (var elem in item)
            {
                elem.DrawOn(graphics);
            }
            Refresh();
        }
        public void ReDraw(ICollection<IGeometry> items,ICollection<IGeometry> selected) {
            Clear();
            foreach (var item in items)
            {
                item.Draw(graphics);
                if (selected.Contains(item)) {
                    item.DrawOn(graphics);
                }
            }
            Refresh();
        }

        private void Clear() {
            this.graphics.Clear(Color.White);
        }
        private void Refresh() {
            this.box.Invalidate();
        }
    }
}
