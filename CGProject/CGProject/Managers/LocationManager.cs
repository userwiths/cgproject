﻿using CGProject.Models;
using CGProject.Models.Abstract;
using CGProject.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGProject.Managers
{
    public static class LocationManager
    {
        public static bool PointInsidePolygon(this Manager manager,out Base polygon, Models.Point point)
        {
            polygon = null;
            if (point == null) { return false; }
            foreach (var group in manager.projectState.Groups)
            {
                if (group is Group grItem)
                {
                    foreach (var component in grItem.GetAllPolygons())
                    {
                        if (component.Contains(point, 2))
                        {
                            polygon = component as Base;
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        public static bool OnPoint(this Manager manager, out Models.Point onPoint, Models.Point point)
        {
            foreach (var group in manager.projectState.Groups)
            {
                if (group is Group grItem)
                {
                    foreach (var component in grItem.GetAllPolygons())
                    {
                        if (component.Contains(point, 1))
                        {
                            foreach (Point pnt in (component as Base).GetPoints())
                            {
                                if (pnt.X >= point.X - 6 &&
                                   pnt.X <= point.X + 6 &&
                                   pnt.Y >= point.Y - 6 &&
                                   pnt.Y <= point.Y + 6
                                )
                                {
                                    onPoint = pnt;
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            onPoint = null;
            return false;
        }
        public static Line OnSideOfPolygon(this Manager manager, out Base polygon, Models.Point point)
        {
            Line temp;
            if (manager.PointInsidePolygon(out polygon, point))
            {
                for (int i = 0; i < polygon.GetPoints().Length - 1; i++)
                {
                    temp = new Line(polygon.GetPoints()[i], polygon.GetPoints()[i + 1]);
                    if (temp.Contains(point, 4))
                    {
                        return temp;
                    }
                }
                temp = new Line(polygon.GetPoints().First(), polygon.GetPoints().Last());
                if (temp.Contains(point, 4))
                {
                    return temp;
                }
            }
            return null;
        }
        public static bool OnPoint(this Manager manager, Models.Point point)
        {
            Models.Point temp = null;
            return manager.OnPoint(out temp, point);
        }
        public static bool OnSide(this Manager manager, Models.Point point)
        {
            Base temp = null;
            return manager.OnSideOfPolygon(out temp, point) != null;
        }
    }
}
