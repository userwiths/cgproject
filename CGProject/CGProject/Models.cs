﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MathNet.Numerics.LinearAlgebra;
using System.Drawing;
using System.Drawing.Drawing2D;
using CGProject.Models.Interfaces;
using CGProject.Models;
using CGProject.Models.Interfaces.Summarized;
using System.Collections;
using System.IO;
using CGProject.Models.Abstract;
using static CGProject.Models.Definition;
using CGProject.Models.Attributes;

namespace CGProject
{ 
    public class Group: IGeometry,ICollection<IGeometry>
    {
        [Search("Contains",PropertyType.Method)]
        public string Name { get; set; }
        public int Id { get => this.GetHashCode(); }
        public IGeometry this[int id] {
            get { return Get(id); }
        }

        private Color _fillColor;
        private Color _borderColor;

        public IList<IGeometry> Components { get; set; }
        Color IColorful.FillColor {
            get => this._fillColor;
            set => this._fillColor = value;
        }
        Color IColorful.BorderColor {
            get => this._borderColor;
            set => this._borderColor = value;
        }

        public Group() {
            Name = this.Id.ToString();
            Components = new List<IGeometry>();
        }
        public Group(IGeometry[] items):this()
        {
            Components = new List<IGeometry>();
            foreach (var element in items)
            {
                Components.Add(element);
            }
        }

        private IGeometry Get(int id) {
            IGeometry result = null;

            if (this.Id == id)
            {
                return this;
            }

            foreach (var comp in Components)
            {
                if (comp.Id == id) {
                    result = comp;
                }else if (comp is Base) {
                    foreach (var point in (comp as Base).GetPoints())
                    {
                        if (point.Id == id) {
                            result = point;
                        }
                    }
                }
                if (result != null) {
                    return result;
                }
            }

            if (result != null) {
                return result;
            }

            foreach (var group in this.GetAllGroups())
            {
                result=(group as Group)[id];
                if (result != null) { return result; }
            }    
            return result;
        }
        public bool Contains(int id, bool recursive = true)
        {
            if (recursive)
            {
                return Get(id) != null;
            }
            return this.Components.Select(x => x.Id).Contains(id);
        }
        public void Remove(int id) {
            bool flag = false;
            foreach (var comp in Components)
            {
                flag = comp.Id == id;
                if(flag){
                    break;
                }
                if (comp is Group) {
                    (comp as Group).Remove(id);
                }
            }
            if (flag) {
                Components = Components.Where(x => x.Id != id).ToList();
            }
        }
        public void Draw(System.Drawing.Graphics gr)
        {
            foreach (var item in this)
            {
                item.Draw(gr);
            }
        }
        
        #region MOVE
        public void MoveWith(PointF margin)
        {
            foreach (var item in this.Components)
            {
                item.MoveWith(margin);
            }
        }
        public void MoveWith(float x, float y)
        {
            foreach (var item in this.Components)
            {
                item.MoveWith(x,y);
            }
        }
        public void MoveTo(PointF margin)
        {
            foreach (var item in this.Components)
            {
                item.MoveTo(margin);
            }
        }
        public void MoveTo(float x, float y)
        {
            foreach (var item in this.Components)
            {
                item.MoveTo(x, y);
            }
        }
        #endregion

        public void Rotate(float angle)
        {
            foreach (var item in this.Components)
            {
                item.Rotate(this.GetCenter().AsNative(),angle);
            }
        }
        public void Rotate(PointF point,float angle)
        {
            foreach (var item in this.Components)
            {
                item.Rotate(point,angle);
            }
        }
        public void Scale(float proportion)
        {
            foreach (var item in this.Components)
            {
                item.Scale(proportion);
            }
        }
        public bool Contains(Models.Point point,int diff=2)
        {
            foreach (var item in this)
            {
                if (item.Contains(point,diff)) {
                    return true;
                }
            }
            return false;
        }
       
        public Models.Point GetCenter()
        {
            Models.Point temp = new Models.Point(0,0);
            int counter = 0;

            foreach (var comp in this.Components)
            {
                temp.X += comp.GetCenter().X;
                temp.Y += comp.GetCenter().Y;

                counter++;
            }
            return new Models.Point(temp.X / counter, temp.Y / counter);
        }

        public void Disown(IGeometry child)
        {
            int id = child.Id;

            if (this.Get(child.Id) == null)
            {
                return;
            }

            Components = Components.Where(x => x.Id != id).ToList();
            foreach (var group in Components.Where(x => x is Group).Select(x => x as Group))
            {
                group.Disown(child);
            }
        }
        public void Disown() {
            this.Components = new List<IGeometry>();
        }

        public List<IGeometry> GetAllPolygons() {
            List<IGeometry> result = new List<IGeometry>();
            foreach (var item in this)
            {
                if (item is Base)
                {
                    result.Add(item);
                }
                else {
                    result.AddRange((item as Group).GetAllPolygons());
                }
            }
            return result;
        }
        public List<IGeometry> GetAllGroups()
        {
            List<IGeometry> result = new List<IGeometry>();
            foreach (var item in this)
            {
                if (item is Group)
                {
                    result.Add(item);
                }
            }
            return result;
        }
        public List<int> GetAll() {
            List<int> result = new List<int>();
            result.Add(this.Id);
            foreach (var item in Components)
            {
                if (item is Base)
                {
                    result.Add(item.Id);
                }
                else {
                    result.AddRange((item as Group).GetAll());
                }
            }
            return result;
        }

        public void DrawOn(System.Drawing.Graphics gr)
        {
            foreach (var item in Components)
            {
                item.DrawOn(gr);
            }
        }
        public object Clone() {
            return new Group() {
                Name = this.Name,
                Components = this.Components.Select(x => x.Clone() as IGeometry).ToList()
            };
        }

        #region ICOLLECTION
        public int Count => this.Components.Count;
        public bool IsReadOnly => false;

        public void Clear()
        {
            foreach (var item in Components)
            {
                if (item is Group gr) {
                    gr.Clear();
                }
            }
            this.Components.Clear();
        }
        public bool Contains(IGeometry item)
        {
            return this.Components.Contains(item);
        }
        public void CopyTo(IGeometry[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }
        public void Add(IGeometry element)
        {
            this.Components.Add(element);
        }
        public bool Remove(IGeometry item)
        {
            return this.Components.Remove(item);
        }
        public IEnumerator<IGeometry> GetEnumerator()
        {
            return new GroupIterator(this);
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new GroupIterator(this);
        }
        #endregion
    }

    public class GroupIterator : IEnumerator<IGeometry> {
        private Group group;
        private int index = -1;
        private IGeometry Current;

        object IEnumerator.Current => this.Current;

        IGeometry IEnumerator<IGeometry>.Current => this.Current;

        public GroupIterator(Group gr) {
            this.group = gr;
        }

        public void Dispose()
        {
            return;
        }

        public bool MoveNext()
        {
            if (++index < group.Components.Count) {
                Current = group.Components[index];
                return true;
            }
            return false;
        }

        public void Reset()
        {
            index = -1;
            Current = group.Components.FirstOrDefault();
        }
    }
}
